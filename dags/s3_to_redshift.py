from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator

from datetime import datetime, timedelta


# default arguments for each task
default_args = {
    'owner': 'nikhil',
    'depends_on_past': False,
    'start_date': datetime(2019, 11, 18),
    'retries': 1,
    'retry_delay': timedelta(minutes=15),
}


dag = DAG('s3_to_redshift',
          default_args=default_args,
          schedule_interval='15 05 * * *')  # "schedule_interval=None" means this dag will only be run by external commands

TEST_BUCKET = 'ist-tcns'
TEST_KEY = 'STOREINV.csv'
LOCAL_FILE = 'temp'
FILE_NAME = 'test.csv'


# simple download task
def download_file(bucket_name, s3_path, dest_path, dest_file_name):
    import boto3
    import os
    from s3fs import S3FileSystem
    client = boto3.client('s3')
    resource = boto3.resource('s3')
    s3system = S3FileSystem()

    print('Local Destination folder :', dest_path)

    if not os.path.exists(dest_path):
        print("creating dest dir")
        os.makedirs(dest_path)

    print('Downloading file at : ', dest_path)
    #resource.Bucket(bucket_name).download_file(s3_path, dest_path)
    client.download_file(bucket_name, s3_path, dest_path + "/" + dest_file_name)
    print('Download Complete')


# simple upload task
def upload_file(to_table, file_path):
    import psycopg2
    #Amazon Redshift connect string
    conn_string = "dbname='***' port='5439' user='***' password='***' host='mycluster.***.redshift.amazonaws.com'"
    #connect to Redshift (database should be open to the world)
    con = psycopg2.connect(conn_string);
    sql="""COPY %s FROM '%s';""" %(to_table, file_path)

    cur = con.cursor()
    cur.execute(sql)
    con.close()


download_from_s3 = PythonOperator(
    task_id='download_from_s3',
    python_callable=download_file,
    depends_on_past=False,
    op_kwargs={'bucket_name': TEST_BUCKET, 's3_path': TEST_KEY, 'dest_path': LOCAL_FILE, 'dest_file_name': FILE_NAME},
    dag=dag)


upload_to_redshift = PythonOperator(
    task_id='upload_to_redshift',
    python_callable=upload_file,
    op_kwargs={'to_table': "test_table", 'file_path' : LOCAL_FILE + "/" + FILE_NAME},
    dag=dag)

download_from_s3 >> upload_to_redshift
