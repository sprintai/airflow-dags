from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator

from datetime import datetime, timedelta


# default arguments for each task
default_args = {
    'owner': 'nikhil',
    'depends_on_past': False,
    'start_date': datetime(2019, 12, 19),
    'retries': 1,
    'retry_delay': timedelta(minutes=15),
}


dag = DAG('trigger_rules_ex',
          default_args=default_args,
          schedule_interval='10 10 * * *')  # "schedule_interval=None" means this dag will only be run by external commands


sleep_task_10 = BashOperator(
    task_id='sleep_for_10',
    bash_command='sleep 10',
    dag=dag)

sleep_task_20 = BashOperator(
    task_id='sleep_for_20',
    bash_command='sleep 20',
    dag=dag)

sleep_task_30 = BashOperator(
    task_id='sleep_for_30',
    bash_command='sleep 30',
    dag=dag)

sleep_task_40 = BashOperator(
    task_id='sleep_for_40',
    bash_command='sleep 40',
    dag=dag)

sleep_task_50 = BashOperator(
    task_id='sleep_for_50',
    bash_command='sleep 50',
    dag=dag)

final_task = BashOperator(
    task_id='sleep_for_final',
    bash_command='echo done',
    trigger_rule='one_success',
    dag=dag)


[sleep_task_10, sleep_task_20, sleep_task_30, sleep_task_40, sleep_task_50] >> final_task
