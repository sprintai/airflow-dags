from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator

from datetime import datetime, timedelta


# default arguments for each task
default_args = {
    'owner': 'nikhil',
    'depends_on_past': False,
    'start_date': datetime(2019, 11, 6),
    'retries': 1,
    'retry_delay': timedelta(minutes=1),
}


dag = DAG('test_s3_download',
          default_args=default_args,
          schedule_interval='28 12 * * *')  # "schedule_interval=None" means this dag will only be run by external commands

TEST_BUCKET = 'ist-tcns'
TEST_KEY = 'STOREINV.csv'
LOCAL_FILE = 'temp'
FILE_NAME = 'test.csv'


# simple download task
def download_file(bucket_name, s3_path, dest_path, dest_file_name):
    import boto3
    import os
    from s3fs import S3FileSystem
    client = boto3.client('s3')
    resource = boto3.resource('s3')
    s3system = S3FileSystem()

    print('Local Destination folder :', dest_path)

    if not os.path.exists(dest_path):
        print("creating dest dir")
        os.makedirs(dest_path)

    print('Downloading file at : ', dest_path)
    #resource.Bucket(bucket_name).download_file(s3_path, dest_path)
    client.download_file(bucket_name, s3_path, dest_path + "/" + dest_file_name)
    print('Download Complete')


# simple upload task
def upload_file(source, bucket, key, file_name):
    import boto3
    print("uploading file")
    s3 = boto3.resource('s3')
    s3.Bucket(bucket).upload_file(source + "/" + file_name, key)
    print("--- uploaded ---")


download_from_s3 = PythonOperator(
    task_id='download_from_s3',
    queue='worker_queue',
    python_callable=download_file,
    op_kwargs={'bucket_name': TEST_BUCKET, 's3_path': TEST_KEY, 'dest_path': LOCAL_FILE, 'dest_file_name': FILE_NAME},
    dag=dag)


sleep_task = BashOperator(
    task_id='sleep_for_1',
    queue='worker_queue',
    bash_command='sleep 1',
    dag=dag)


upload_to_s3 = PythonOperator(
    task_id='upload_to_s3',
    queue='worker_queue',
    python_callable=upload_file,
    op_kwargs={'bucket': TEST_BUCKET, 'key': TEST_KEY, 'source': LOCAL_FILE, 'file_name' : FILE_NAME},
    dag=dag)


#download_from_s3.set_downstream(sleep_task)
#sleep_task.set_downstream(upload_to_s3)

download_from_s3 >> sleep_task >> upload_to_s3
