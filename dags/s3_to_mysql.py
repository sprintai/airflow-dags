from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator

from datetime import datetime, timedelta


# default arguments for each task
default_args = {
    'owner': 'nikhil',
    'depends_on_past': False,
    'start_date': datetime(2019, 11, 18),
    'retries': 1,
    'retry_delay': timedelta(minutes=15),
}


dag = DAG('s3_to_mysql',
          default_args=default_args,
          schedule_interval='15 05 * * *')  # "schedule_interval=None" means this dag will only be run by external commands

TEST_BUCKET = 'ist-tcns'
TEST_KEY = 'STOREINV.csv'
LOCAL_FILE = 'temp'
FILE_NAME = 'test.csv'


# simple download task
def download_file(bucket_name, s3_path, dest_path, dest_file_name):
    import boto3
    import os
    from s3fs import S3FileSystem
    client = boto3.client('s3')
    resource = boto3.resource('s3')
    s3system = S3FileSystem()

    print('Local Destination folder :', dest_path)

    if not os.path.exists(dest_path):
        print("creating dest dir")
        os.makedirs(dest_path)

    print('Downloading file at : ', dest_path)
    #resource.Bucket(bucket_name).download_file(s3_path, dest_path)
    client.download_file(bucket_name, s3_path, dest_path + "/" + dest_file_name)
    print('Download Complete')


# simple upload task
def upload_file(file_path):
    import csv
    import MySQLdb

    mydb = MySQLdb.connect(host='localhost',
        user='root',
        passwd='',
        db='mydb')
    cursor = mydb.cursor()

    csv_data = csv.reader(file(file_path))

    for row in csv_data:
        cursor.execute('INSERT INTO test_table (company, year, revenue ) VALUES("%s", "%s", "%s")',row)

    #close the connection to the database.
    mydb.commit()
    cursor.close()


download_from_s3 = PythonOperator(
    task_id='download_from_s3',
    python_callable=download_file,
    depends_on_past=False,
    op_kwargs={'bucket_name': TEST_BUCKET, 's3_path': TEST_KEY, 'dest_path': LOCAL_FILE, 'dest_file_name': FILE_NAME},
    dag=dag)


upload_to_mysql = PythonOperator(
    task_id='upload_to_mysql',
    python_callable=upload_file,
    op_kwargs={'file_path' : LOCAL_FILE + "/" + FILE_NAME},
    dag=dag)

download_from_s3 >> upload_to_mysql
