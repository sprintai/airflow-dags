from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator
from airflow.operators.docker_operator import DockerOperator

from datetime import datetime, timedelta


# default arguments for each task
default_args = {
    'owner': 'nikhil',
    'depends_on_past': False,
    'start_date': datetime(2019, 12, 1),
    'retries': 1,
    'retry_delay': timedelta(minutes=15),
}


dag = DAG('docker_test',
          default_args=default_args,
          schedule_interval='10 16 * * *')  # "schedule_interval=None" means this dag will only be run by external commands


t2 = DockerOperator(
                task_id='download_docker_command',
                queue='worker_queue',
                image='sprintai/download_from_s3:test',
                command=None,
                dag=dag
        )

t2
