from datetime import datetime
from airflow import DAG
import json

from airflow.contrib.operators.gcp_bigtable_operator import BigtableInstanceCreateOperator, BigtableTableCreateOperator
from airflow.contrib.operators.bigquery_operator import BigQueryCreateEmptyTableOperator
from airflow.operators.python_operator import PythonOperator
from airflow.contrib.operators.gcs_to_bq import GoogleCloudStorageToBigQueryOperator

dag = DAG('gcp_operators_dag', description='Another tutorial DAG',
          schedule_interval='0 12 * * *',
          start_date=datetime(2020, 1, 12), catchup=False)

create_instance_task = BigtableInstanceCreateOperator(
    project_id='airflow-264717',
    instance_id='nikhil-instance-test',
    main_cluster_id='nikhiltest',
    main_cluster_zone='asia-south1-a',
    instance_display_name='nikhil-instance-display',
    instance_type=int(2),
    instance_labels={},
    #cluster_nodes=int(1),
    cluster_storage_type=int(1),
    task_id='create_instance_task',
    dag=dag
)

create_table_task = BigtableTableCreateOperator(
    project_id='airflow-264717',
    instance_id='nikhil-instance-test',
    table_id='nikhil-table',
    task_id='create_table_task',
    dag=dag
)

create_instance_task >> create_table_task

CreateTable = BigQueryCreateEmptyTableOperator(
                task_id='BigQueryCreateEmptyTableOperator_task',
                dataset_id='nikhil_dataset',
                table_id='nikhil_bq_table',
                project_id='airflow-264717',
                schema_fields=[{"name": "emp_name", "type": "STRING", "mode": "REQUIRED"},
                               {"name": "salary", "type": "INTEGER", "mode": "NULLABLE"}],
                bigquery_conn_id='google_cloud_default',
                google_cloud_storage_conn_id='google_cloud_default',
                dag=dag
            )

gcs_to_bq = GoogleCloudStorageToBigQueryOperator(
        task_id='gcs_to_table',
        bucket='airflow-test-nikhil',
        source_objects=[
            'test.csv'
        ],
        destination_project_dataset_table='airflow-264717.nikhil_dataset.nikhil_bq_table',
        create_disposition='CREATE_IF_NEEDED',
        write_disposition='WRITE_TRUNCATE',
        google_cloud_storage_conn_id='google_cloud_default',
        bigquery_conn_id='google_cloud_default',
        dag=dag
    )
