from datetime import datetime
from airflow import DAG

from airflow.contrib.hooks.gcs_hook import GoogleCloudStorageHook
from airflow.operators.python_operator import PythonOperator

dag = DAG('gcp_crud_dag', description='Another tutorial DAG',
          schedule_interval='0 12 * * *',
          start_date=datetime(2020, 1, 10), catchup=False)

def gcp_crud():
    print("-- GCP CRUD --")

    GoogleCloudStorageHook().upload(bucket='airflow-test-nikhil',
                                    object='test_folder/download_file.txt',
                                    filename='/home/nikhil/airflow/temp/downloaded.txt')

    GoogleCloudStorageHook().download(bucket='airflow-test-nikhil',
                                        object='test_folder/test_file.txt',
                                        filename='/home/nikhil/airflow/temp/downloaded.txt')

    file_exists = GoogleCloudStorageHook().exists(bucket='airflow-test-nikhil',
                                        object='test_folder/test_file.txt')

    print(file_exists)

    GoogleCloudStorageHook().delete(bucket='airflow-test-nikhil',
                                object='test_file.txt')

    GoogleCloudStorageHook().get_size(bucket='airflow-test-nikhil',
                                object='test_folder/test_file.txt')

    objects_list = GoogleCloudStorageHook().list(bucket='airflow-test-nikhil')

    print(objects_list)

    print("-- Done with GCP CRUD task --")

gcp_task = PythonOperator(task_id='gcp_hook',python_callable=gcp_crud,dag=dag)

gcp_task
