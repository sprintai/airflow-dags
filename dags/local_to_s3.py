from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator

from datetime import datetime, timedelta


# default arguments for each task
default_args = {
    'owner': 'nikhil',
    'depends_on_past': False,
    'start_date': datetime(2019, 11, 21),
    'retries': 1,
    'retry_delay': timedelta(minutes=15),
}


dag = DAG('local_to_s3',
          default_args=default_args,
          schedule_interval='55 10 * * *')  # "schedule_interval=None" means this dag will only be run by external commands

BUCKET_NAME = 'ist-tcns'
S3_FILE_KEY = 'STOREINV.csv'
LOCAL_FILE_PATH = 'temp'
LOCAL_FILE_NAME = 'test.csv'


# simple upload task
def upload_file(source, bucket, key, file_name):
    import boto3
    print("uploading file")
    s3 = boto3.resource('s3')
    s3.Bucket(bucket).upload_file(source + "/" + file_name, key)
    print("--- uploaded ---")

upload_to_s3 = PythonOperator(
    task_id='upload_to_s3',
    python_callable=upload_file,
    op_kwargs={'bucket': BUCKET_NAME, 'key': S3_FILE_KEY, 'source': LOCAL_FILE_PATH, 'file_name' : LOCAL_FILE_NAME},
    dag=dag)

upload_to_s3
