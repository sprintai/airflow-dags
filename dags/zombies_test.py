from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator

from datetime import datetime, timedelta


# default arguments for each task
default_args = {
    'owner': 'nikhil',
    'depends_on_past': False,
    'start_date': datetime(2019, 12, 29),
    'retries': 1,
    'retry_delay': timedelta(minutes=15),
}


dag = DAG('zombies_test',
          default_args=default_args,
          schedule_interval='10 10 * * *')  # "schedule_interval=None" means this dag will only be run by external commands


sleep_task = BashOperator(
    task_id='sleep_for_1',
    bash_command='sleep 120',
    dag=dag)

sleep_task
