from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator
from airflow.operators.docker_operator import DockerOperator

from datetime import datetime, timedelta


# default arguments for each task
default_args = {
    'owner': 'nikhil',
    'depends_on_past': False,
    'start_date': datetime(2020, 4, 30),
    'retries': 1,
    'retry_delay': timedelta(minutes=45),
}


dag = DAG('feature_creator',
          default_args=default_args,
          schedule_interval='10 16 * * *')  # "schedule_interval=None" means this dag will only be run by external commands


t2 = DockerOperator(
                task_id='feature_creator_single_task',
                image='435747621803.dkr.ecr.ap-south-1.amazonaws.com/feature_creator:dev',
                command=None,
                dag=dag
        )

t2
