from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator
from airflow.operators.docker_operator import DockerOperator

from datetime import datetime, timedelta

import time
import boto3.ec2
from fabric.api import env
from fabric.api import execute
from fabric.api import sudo
from fabric.api import run
from fabric.api import put


env.aws_region = 'ap-south-1'
image_id = 'ami-0123b531fc646552f'
instance_type = 't2.micro'
keypair_name = 'airflow-server'
server_name = "airflow-test-worker"
security_group = "airflow-workers"
conn = boto3.client('ec2', region_name=env.aws_region)

# default arguments for each task
default_args = {
    'owner': 'nikhil',
    'depends_on_past': False,
    'start_date': datetime(2019, 12, 10),
    'retries': 1,
    'retry_delay': timedelta(minutes=15),
}


dag = DAG('fabric_demo',
          default_args=default_args,
          schedule_interval='10 16 * * *')  # "schedule_interval=None" means this dag will only be run by external commands

def get_ec2_connection():
    if 'ec2' not in env:
        if conn is not None:
            env.ec2 = conn
            print("Connected to EC2 region :", env.aws_region)
        else:
            msg = "Unable to connect to EC2 region"
            print(msg)
    return env.ec2

def create_ec2_instance(image_id, instance_type, keypair_name, security_group):
    # Provision and launch the EC2 instance

    #ec2_client = boto3.client('ec2')
    conn = get_ec2_connection()
    try:
        user_data = '''#!/bin/bash
        echo 'test' > /tmp/hello
        sudo su - ubuntu
        sudo apt-get update
        sudo apt install python3-pip --yes
        sudo -u ubuntu export AIRFLOW_HOME=~/airflow
        pip3 install apache-airflow
        pip3 install boto3
        sudo -u ubuntu airflow initdb
        sudo rm -rf /home/ubuntu/airflow/airflow.cfg
        sudo apt  install awscli --yes
        aws s3 cp s3://ist-tcns/airflow.cfg /home/ubuntu/airflow/airflow.cfg
        pip3 install psycopg2-binary
        pip3 install celery
        airflow initdb
        sudo apt-get remove docker docker-engine docker.io
        sudo apt install docker.io --yes
        sudo systemctl start docker
        sudo systemctl enable docker
        ls -lrth /var/run/docker.sock
        sudo chmod 666 /var/run/docker.sock
        ls -lrth /var/run/docker.sock
        pip3 install flower
        sudo -u ubuntu mkdir /home/ubuntu/airflow/dags
        aws s3 sync s3://ist-tcns/dags /home/ubuntu/airflow/dags
        sudo -u ubuntu nohup airflow worker -q worker_queue &> /dev/null &
        sudo -u ubuntu nohup airflow flower &> /dev/null &'''
        reservations = conn.run_instances(ImageId=image_id,
                                            InstanceType=instance_type,
                                            KeyName=keypair_name,
                                            SecurityGroups=[security_group, ],
                                            UserData = user_data,
                                            IamInstanceProfile={
                                                    'Arn': 'arn:aws:iam::435747621803:instance-profile/ecsInstanceRole'
                                             },
                                            MinCount=1,
                                            MaxCount=1)

        print("ec2 instance created")

    except Exception as e:
        print(e)
        return None
    return reservations['Instances'][0]

def launch_ec2_instance():

    public_dns = ""

    instance_info = create_ec2_instance(image_id, instance_type, keypair_name, security_group)
    if instance_info is not None:
        print(instance_info)
        instance_id = instance_info["InstanceId"]
        print(instance_info["InstanceId"])

        while True:

            time.sleep(7)
            print("checking status for :", instance_id)
            ec2_instance = conn.describe_instance_status(InstanceIds=[instance_id])

            if len(ec2_instance['InstanceStatuses']) == 0:
                continue

            print(ec2_instance)
            ec2_status = ec2_instance['InstanceStatuses'][0]['InstanceState']['Name']
            print(ec2_status)

            if ec2_status == "running":
                while True:

                    time.sleep(5)
                    print("getting dns for :", instance_id)
                    ec2_instance = conn.describe_instances(InstanceIds=[instance_id])
                    print(ec2_instance)

                    if len(ec2_instance['Reservations'][0]['Instances']) == 0:
                        continue

                    public_dns = ec2_instance['Reservations'][0]['Instances'][0]['PublicDnsName']
                    print(public_dns)
                    break

                break
            else:
                print("ec2 status :", ec2_status)

    return public_dns


def start_fabric_task():
    print("-- test function --")
    host_public_dns = launch_ec2_instance()
    print("-- ec2 running --")

fabric_task = PythonOperator(
    task_id='fabric_task',
    python_callable=start_fabric_task,
    op_kwargs={},
    dag=dag)

t1 = DockerOperator(
                task_id='download_docker_command_1',
                queue='worker_queue',
                image='sprintai/download_from_s3:test',
                command=None,
                dag=dag
        )

t2 = DockerOperator(
                task_id='download_docker_command_2',
                queue='worker_queue',
                image='sprintai/download_from_s3:test',
                command=None,
                dag=dag
        )

t3 = DockerOperator(
                task_id='download_docker_command_3',
                queue='worker_queue',
                image='sprintai/download_from_s3:test',
                command=None,
                dag=dag
        )

t4 = DockerOperator(
                task_id='download_docker_command_4',
                queue='worker_queue',
                image='sprintai/download_from_s3:test',
                command=None,
                dag=dag
        )

fabric_task >> [t1, t2, t3, t4]
