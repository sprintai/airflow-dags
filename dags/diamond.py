from datetime import datetime
from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator
from airflow.operators import DataAppendOperator

dag = DAG('my_test_dag', description='Another tutorial DAG',
          schedule_interval='0 12 * * *',
          start_date=datetime(2019, 12, 20), catchup=False)


#dummy_task = DummyOperator(task_id='dummy_task', dag=dag)

#sensor_task = MyFirstSensor(task_id='my_sensor_task', poke_interval=30, dag=dag)

operator_task = DataAppendOperator(
    task_id='data_append_operator_task',
    source_file_path = '/home/nikhil/airflow/temp/source.txt',
    dest_file_path='/home/nikhil/airflow/temp/dest.txt',
    delete_list = ['nikhil', 'boy'],
    dag=dag)

#hook_task = PythonOperator(task_id='test_hook',python_callable=test_hook,dag=dag)

operator_task
