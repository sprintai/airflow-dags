provider "aws" {
  profile    = "default"
  region     = "ap-south-1"
}

resource "aws_instance" "example" {
  ami             = "ami-0123b531fc646552f"
  instance_type   = "t2.micro"
  security_groups = ["${aws_security_group.example.name}"]
  key_name        = "${aws_key_pair.generated_key.key_name}"

  provisioner "local-exec" {
        command = "scp -i sample-key-pair.pem -r /home/nikhil/Desktop/shiva/fwdnotes ubuntu@${aws_instance.example.public_ip}:/home/ubuntu"
  }

    connection {
      type        = "ssh"
      host        = aws_instance.example.public_ip
      private_key = "${tls_private_key.example.private_key_pem}"
      user        = "ubuntu"
      timeout     = "1m"
    }
}

resource "tls_private_key" "example" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "generated_key" {
  key_name   = "sample-key-pair"
  public_key = "${tls_private_key.example.public_key_openssh}"
}

resource "aws_security_group" "example" {
  name        = "grant ssh"
  description = "grant ssh"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
