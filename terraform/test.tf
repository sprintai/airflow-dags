provider "aws" {
  profile    = "default"
  region     = "ap-south-1"
}

data "aws_vpc" "default_vpc" {
  id = "vpc-0fd6e367"
}

resource "aws_security_group" "sphinx-sg" {
  name        = "sphinx-instance-sg"
  description = "security group for sphinx instances"
  vpc_id      = "vpc-0fd6e367"
}


resource "aws_security_group_rule" "sphinx_instance_sg_ingress_1" {
  # allow ssh to instance within cidr block
  type              = "ingress"
  description       = "allow ssh to sphinx instance within cidr block"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.sphinx-sg.id
}
resource "aws_security_group_rule" "sphinx_instance_sg_ingress_2" {
  # allow ssh to instance within cidr block
  type              = "ingress"
  description       = "allow 80 to sphinx instance within cidr block"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.sphinx-sg.id
}

resource "aws_security_group_rule" "sphinx_instance_sg_egress_1" {
  # allow ssh to instance within cidr block
  type              = "egress"
  description       = "allow 80 to  instance within cidr block"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.sphinx-sg.id
}

resource "aws_security_group_rule" "sphinx_instance_sg_egress_2" {
  # allow ssh to instance within cidr block
  type              = "egress"
  description       = "allow 443 to  instance within cidr block"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.sphinx-sg.id
}

resource "aws_instance" "sphinx-ec2" {
  ami         = "ami-0123b531fc646552f"
  instance_type = "t2.micro"
  subnet_id = "subnet-9f6634f7"
  security_groups = [aws_security_group.sphinx-sg.id]
  key_name = "aws-ec2"

  provisioner "local-exec" {
        command = "scp -i /home/nikhil/Desktop/Projects/aws-ec2.pem -r /home/nikhil/Desktop/shiva/fwdnotes ubuntu@${aws_instance.sphinx-ec2.public_ip}:/home/ubuntu"
  }

   connection {
     host     = aws_instance.sphinx-ec2.public_ip
     type     = "ssh"
     user     = "ubuntu"
     private_key = file("/home/nikhil/Desktop/Projects/aws-ec2.pem")
   }

  tags = {
    Name = "terraform-ubuntu"
  }
}
