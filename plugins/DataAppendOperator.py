import logging

from airflow.models import BaseOperator
from airflow.plugins_manager import AirflowPlugin
from airflow.utils.decorators import apply_defaults

log = logging.getLogger(__name__)

class DataAppendOperator(BaseOperator):

    @apply_defaults
    def __init__(self, source_file_path, dest_file_path, delete_list, *args, **kwargs):
        self.source_file_path = source_file_path
        self.dest_file_path = dest_file_path
        self.delete_list = delete_list
        super(DataAppendOperator, self).__init__(*args, **kwargs)

    def execute(self, context):
        log.info("-- starting Data Operator custom operator --")
        log.info('source_file_path: %s', self.source_file_path)
        log.info('dest_file_path: %s', self.dest_file_path)
        log.info('dest_file_path: %s', self.delete_list)

        SourceFilePath = self.source_file_path
        DestFilePath = self.dest_file_path
        DeleteList = self.delete_list

        fin = open(SourceFilePath)
        fout = open(DestFilePath, "a")
        for line in fin:
            log.info('### reading line: %s', line)
            for word in DeleteList:
                log.info('*** checking word: %s', word)
                line = line.replace(word, "")

            log.info('### line converted to: %s', line)
            fout.write(line)
        fin.close()
        fout.close()

class DataAppendPlugin(AirflowPlugin):
    name = "data_append_plugin"
    operators = [DataAppendOperator]
