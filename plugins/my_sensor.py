import logging
from datetime import datetime
import os
import stat

from airflow.contrib.hooks.fs_hook import FSHook
from airflow.operators.sensors import BaseSensorOperator
from airflow.plugins_manager import AirflowPlugin
from airflow.utils.decorators import apply_defaults

log = logging.getLogger(__name__)

class MyFirstSensor(BaseSensorOperator):

    @apply_defaults
    def __init__(self, filepath, fs_conn_id='fs_default',*args, **kwargs):
        super(MyFirstSensor, self).__init__(*args, **kwargs)
        self.filepath = filepath
        self.fs_conn_id = fs_conn_id

    def poke(self, context):

        hook = FSHook(self.fs_conn_id)
        basepath = hook.get_path()
        full_path = os.path.join(basepath, self.filepath)
        self.log.info('Poking for file %s', full_path)
        try:
            if stat.S_ISDIR(os.stat(full_path).st_mode):
                for root, dirs, files in os.walk(full_path):
                    print(len(files))
                    if len(files) >= 5:
                        return True
            else:
                # full_path was a file directly
                return True
        except OSError:
            return False
        return False

class MyFirstPlugin(AirflowPlugin):
    name = "my_first_sensor"
    operators = [MyFirstSensor]
