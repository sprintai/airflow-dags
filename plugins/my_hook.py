import logging
from datetime import datetime
from airflow.hooks.base_hook import BaseHook
from airflow.plugins_manager import AirflowPlugin
from airflow.utils.decorators import apply_defaults
from airflow.hooks.mysql_hook import MySqlHook
from airflow.hooks.postgres_hook import PostgresHook

log = logging.getLogger(__name__)

class MysqlToPostgres(BaseHook):

    def __init__(self):
        print("intalise function")

    def copy_table(self, postgres_conn_id, mysql_conn_id):
        print("-- getting records from mysql --")

        mysqlserver = MySqlHook(mysql_conn_id)
        sql_query = "SELECT * from employees "
        data = mysqlserver.get_records(sql_query)

        postgresserver = PostgresHook(postgres_conn_id)
        postgres_query = "INSERT INTO employees VALUES(%s, %s, %s);"
        postgresserver.insert_rows(table='employees', rows=data)

        #sql = "SELECT * from employees "
        #row_count = postgresserver.get_records(sql)
        #print(row_count[0][1], row_count[0][2])

        return True

class MyFirstPlugin(AirflowPlugin):
    name = "mysql_to_postgres"
    operators = [MysqlToPostgres]
